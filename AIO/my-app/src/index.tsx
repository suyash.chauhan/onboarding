import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Create from './routes/Create'
import Edit from './routes/Edit'
import View from './routes/View'
import Views from './routes/Views'

ReactDOM.render(
  <BrowserRouter>
    <Routes>
      {/* <Route path="/" element></Route> */}
      {/* <Route path="/" element={<Navbar />}> */}
      <Route path="/" element={<App />} >
        <Route path="edit/:userId" element={<Edit />} />
        <Route index element={<Views />} />
        {/* </Route> */}
        <Route path="/views/:userId" element={<View />} />
        <Route path="/create" element={<Create />} />
      </Route>
      <Route path="*" element={<div>Hello</div>} />

    </Routes>
  </BrowserRouter>,
  document.getElementById('root')
);
