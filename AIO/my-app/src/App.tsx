import React from 'react';
import './App.css';
import { Link, Outlet } from 'react-router-dom';

interface State {
  users: Array<User> | []
}
interface User {
  id: number,
  name: string,
  address: string,
  email: string,
}
export default class App extends React.Component {

  state: State = {
    users: []
  }
  componentDidMount() {
    fetch('http://localhost:3001/users').then(res => res.json()).then(res => this.setState({ users: res })).catch(err => console.log(err))
  }
  deleteUser = async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, id: number) => {
    await fetch(`http://localhost:3001/users/${id}`, { method: 'DELETE' }).then(res => console.log(res)).catch(err => console.log(err))
    await fetch('http://localhost:3001/users').then(res => res.json()).then(res => this.setState({ users: res })).catch(err => console.log(err))
  }

  render() {
    return (<div >
      <div className='nav'>
        <ul className='styledList'>
          <li><Link className='styledLinks' to={`/`}>Home</Link></li>
          <li><Link className = 'styledLinks' to={`/create`}>Add New User</Link></li>
        </ul>
      </div>
      <Outlet />
      {/* {this.state.users.map(item => (
        <div className='seperateUser' key={item.id}>
          <h1>{item.name}</h1>
          <Link to={`/edit/${item.id}`}>Edit</Link>
          <Link to={`/view/${item.id}`}>View</Link>
          <button onClick={(e) => this.deleteUser(e, item.id)}>Delete</button>
        </div>
      ))} */}

    </div>);
  }
}

