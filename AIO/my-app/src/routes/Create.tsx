import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

const defaultFormData = {
  id: 0,
  name: '',
  address: '',
  email: ''
}
function Create() {
  const [formData, setFormData] = useState(defaultFormData)
  const navigate =  useNavigate()
  function onChange(e: React.ChangeEvent<HTMLInputElement>) {
    setFormData((prevState) => ({
      ...prevState,
      [e.target.id]: e.target.value
    }))
  }
  function handleSubmit(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault()
    fetch('http://localhost:3001/users/', {
      method: 'POST', headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }, body: JSON.stringify(formData)
    }).then(res => navigate('/')).catch(err => console.log(err))
  }
  return <div className='container'>
    <form onSubmit={handleSubmit}>
      <label htmlFor="name">Name : </label>
      <input id="name" type="text" value={formData.name} onChange={onChange} />
      <br />
      <br />
      <label htmlFor="address">Address : </label>
      <input id="address" type="text" value={formData.address} onChange={onChange} />
      <br />
      <br />
      <label htmlFor="email">Email : </label>
      <input id="email" type="text" value={formData.email} onChange={onChange} />
      <br />
      <br />
      <button className='button submitButton' type="submit">Submit</button>
    </form>
  </div>;
}

export default Create;
