import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useState } from 'react'

interface User {
    id: number,
    name: string,
    address: string,
    email: string,
}
function Views() {
    const [userData, setUserData] = useState<Array<User> | null>(null)
    useEffect(() => {
        fetch(`http://localhost:3001/users`).then(res => res.json()).then(res => setUserData(res)).catch(err => console.log(err))
    }, [])
    const deleteUser = async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>, id: number) => {
        await fetch(`http://localhost:3001/users/${id}`, { method: 'DELETE' }).then(res => console.log(res)).catch(err => console.log(err))
        await fetch('http://localhost:3001/users').then(res => res.json()).then(res => setUserData(res)).catch(err => console.log(err))
    }
    return (
        <div className='container'>
            <table>
                <tr><th>UserName </th><th>Actions Available</th></tr>
                {userData && userData.map(item => (
                    <tr>
                        <td><h3>{item.name}</h3></td>
                        <td><Link className='styledLinks edit' to={`/edit/${item.id}`}>Edit</Link>
                            <Link className='styledLinks view' to={`/views/${item.id}`}>View</Link>
                            <button className='button deleteButton' onClick={(e) => deleteUser(e, item.id)}>Delete</button>
                        </td>

                    </tr>
                ))
                }
            </table>
        </div >
    )

}

export default Views;
