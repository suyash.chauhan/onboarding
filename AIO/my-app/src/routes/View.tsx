import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useState } from 'react'

interface User {
    id: number,
    name: string,
    address: string,
    email: string,
}
function View() {
    const [userData, setUserData] = useState<User | null>(null)
    const params = useParams()
    useEffect(() => {
        fetch(`http://localhost:3001/users/${params.userId}`).then(res => res.json()).then(res => setUserData(res)).catch(err => console.log(err))
    }, [])
    return (
        userData &&
        <div className='container'>
            <h1>Hi, My name is {userData.name} :) </h1>
            <p>My Address is {userData.address}</p>
            <h4>You can Contact me <h2>{userData.email}</h2></h4>
        </div>);
}

export default View;
